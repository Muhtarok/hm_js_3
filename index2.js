"use strict"

// 2. Напишіть програму, яка запитує у користувача місяць року (українською мовою маленкими літерами) та виводить повідомлення, 
// скільки днів у цьому місяці. Результат виводиться в консоль.
// Скільки днів в кожному місяці можна знайти тут в розділі Юліанський і Григоріанський календарі - https://uk.wikipedia.org/wiki/%D0%9C%D1%96%D1%81%D1%8F%D1%86%D1%8C_(%D1%87%D0%B0%D1%81)
// (Використайте switch case)

let userMonth = prompt("Enter the month name");

switch (userMonth) {
    case "січень":
        console.log("у цьому місяці 31 день");
        break;
    case "лютий":
        console.log("у цьому місяці 28 днів");
        break;
    case "березень":
        console.log("у цьому місяці 31 день");
        break;
    case "квітень":
        console.log("у цьому місяці 30 днів");
        break;
    case "травень":
        console.log("у цьому місяці 31 день");
        break;
    case "червень":
        console.log("у цьому місяці 30 днів");
        break;
    case "липень":
        console.log("у цьому місяці 31 день");
        break;
    case "серпень":
        console.log("у цьому місяці 31 день");
        break;
    case "вересень":
        console.log("у цьому місяці 30 днів");
        break;
    case "жовтень":
        console.log("у цьому місяці 31 день");
        break;
    case "листопад":
        console.log("у цьому місяці 30 днів");
        break;
    case "грудень":
        console.log("у цьому місяці 31 день");
        break;
    default:
        console.log("Ви ввели не місяць");
}